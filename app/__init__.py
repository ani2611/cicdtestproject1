from flask import Flask
from pymongo import MongoClient

db = None


def create_app(test_config=None):
    global db
    app = Flask(__name__)

    if test_config:
        app.config.update(test_config)
    else:
        app.config.from_pyfile("config.py", silent=True)

    client = MongoClient(
        app.config.get("MONGO_URI", "mongodb://localhost:27017/pythondb")
    )
    db = client[app.config.get("DB_NAME", "user_management")]

    from .routes import main

    app.register_blueprint(main)

    return app
